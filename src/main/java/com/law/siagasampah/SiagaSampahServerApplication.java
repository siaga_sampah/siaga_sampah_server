package com.law.siagasampah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableAsync
public class SiagaSampahServerApplication extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SiagaSampahServerApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SiagaSampahServerApplication.class, args);
	}

	@Bean
	public Docket api(){
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build();
	}

	@Bean
	public Docket usersApi(){
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("users")
				.select()
				.paths(PathSelectors.regex("/users.*"))
				.build();
	}

	@Bean
	public Docket trashApi(){
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("trash")
				.select()
				.paths(PathSelectors.regex("/trash.*"))
				.build();
	}

	private ApiInfo apiInfo(){
		return new ApiInfoBuilder()
				.title("Siaga Sampah REST API")
				.description("Siaga Sampah REST APIs Documentations")
				.version("1.0")
				.build();
	}
}
