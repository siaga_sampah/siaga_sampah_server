package com.law.siagasampah.repository;

import com.law.siagasampah.domain.AccessToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by arief on 28/03/2016.
 */
@Repository
public interface AccessTokenRepository extends CrudRepository<AccessToken, Integer>{

    public AccessToken findByToken(String token);

    public AccessToken findByClientId(String clientId);
}
