package com.law.siagasampah.repository;

import com.law.siagasampah.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by arsia on 11-Apr-16.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
    public User findById(int id);

    public User findByEmail(String email);

    public User findByEmailAndPassword(String email, String password);

    @Query("SELECT name FROM User WHERE id = :id")
    public String findNameById(@Param("id")int id);
}
