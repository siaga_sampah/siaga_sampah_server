package com.law.siagasampah.repository;

import com.law.siagasampah.domain.Location;
import com.law.siagasampah.domain.Trash;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by arsia on 19-Apr-16.
 */
@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {
    @Query(value = "SELECT *, ( 3959 * acos( cos( radians(:lat) ) * cos( radians( l.lat ) ) * cos( radians( l.lng ) - radians(:lng) ) + sin( radians(:lat) ) * sin( radians( l.lat ) ) ) ) AS distance FROM location l, trash t WHERE t.status = 0 AND t.id = l.trash_id HAVING distance < 0.621371 ORDER BY distance LIMIT 20",  nativeQuery = true)
    public List<Location> findByPageable(@Param("lat")double lat, @Param("lng")double lng);
}
