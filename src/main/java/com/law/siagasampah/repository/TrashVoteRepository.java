package com.law.siagasampah.repository;

import com.law.siagasampah.domain.Trash;
import com.law.siagasampah.domain.TrashVote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by arsia on 26-Apr-16.
 */
@Repository
public interface TrashVoteRepository extends CrudRepository<TrashVote, String>{
    public TrashVote findByUserIdAndTrashId(int userId, String trashId);
}
