package com.law.siagasampah.repository;

import com.law.siagasampah.domain.Trash;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by arsia on 18-Apr-16.
 */
@Repository
public interface TrashRepository extends CrudRepository<Trash, String> {
    @Query("SELECT r FROM Trash r WHERE status = 0 ORDER BY reportDate DESC")
    public List<Trash> findByPageable(Pageable pageable);

    @Query("SELECT r FROM Trash r WHERE userId = :userId AND status !=2 ORDER BY reportDate DESC")
    public List<Trash> findByUserIdAndPageable(@Param("userId")int userId, Pageable pageable);
}
