package com.law.siagasampah.domain;

import javax.persistence.*;

/**
 * Created by arsia on 11-Apr-16.
 */
@Entity
@Table(name = "access_token", schema = "siaga_sampah")
public class AccessToken {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "client_id")
    private String clientId;
    @Basic
    @Column(name = "token")
    private String token;

    public AccessToken() {
    }

    public AccessToken(String token, String clientId){
        this.token = token;
        this.clientId = clientId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessToken that = (AccessToken) o;

        if (id != that.id) return false;
        if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) return false;
        if (token != null ? !token.equals(that.token) : that.token != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        return result;
    }
}
