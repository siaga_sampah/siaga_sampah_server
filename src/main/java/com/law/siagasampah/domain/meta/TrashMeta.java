package com.law.siagasampah.domain.meta;

import com.law.siagasampah.domain.Location;
import io.swagger.annotations.ApiModelProperty;
import java.sql.Timestamp;

/**
 * Created by arsia on 19-Apr-16.
 */
public class TrashMeta {
    private String id;
    private String image;
    private int vote;
    @ApiModelProperty(dataType = "java.lang.String", example = "yyyy-MM-dd HH:mm:ss", value = "yyyy-MM-dd HH:mm:ss")
    private Timestamp reportDate;
    private int status;
    private Location location;
    private String userName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public Timestamp getReportDate() {
        return reportDate;
    }

    public void setReportDate(Timestamp reportDate) {
        this.reportDate = reportDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
