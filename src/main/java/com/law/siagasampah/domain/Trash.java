package com.law.siagasampah.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by arsia on 11-Apr-16.
 */
@Entity
@Table(name = "trash", schema = "siaga_sampah")
public class Trash {
    @Id
    @Column(name = "id", nullable = false)
    private String id;
    @Basic
    @Column(name = "image")
    private String image;
    @Basic
    @Column(name = "description")
    private String description;
    @Basic
    @Column(name = "vote")
    private int vote;
    @Basic
    @Column(name = "report_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(dataType = "java.lang.String", example = "yyyy-MM-dd HH:mm:ss", value = "yyyy-MM-dd HH:mm:ss")
    private Timestamp reportDate;
    @Basic
    @Column(name = "status")
    private int status;

    @OneToOne
    @JoinColumn(name = "id")
    private Location location;

    @Basic
    @Column(name = "user_id")
    private int userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public Timestamp getReportDate() {
        return reportDate;
    }

    public void setReportDate(Timestamp reportDate) {
        this.reportDate = reportDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trash trash = (Trash) o;

        if (id != null ? !id.equals(trash.id) : trash.id != null) return false;
        if (vote != trash.vote) return false;
        if (status != trash.status) return false;
        if (image != null ? !image.equals(trash.image) : trash.image != null) return false;
        if (description != null ? !description.equals(trash.description) : trash.description != null) return false;
        if (reportDate != null ? !reportDate.equals(trash.reportDate) : trash.reportDate != null) return false;
        if (userId != trash.userId) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + vote;
        result = 31 * result + (reportDate != null ? reportDate.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + userId;
        return result;
    }
}
