package com.law.siagasampah.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by arsia on 11-Apr-16.
 */
public class TrashVotePK implements Serializable {

    @Column(name = "user_id")
    @Id
    private int userId;

    @Column(name = "trash_id")
    @Id
    private String trashId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTrashId() {
        return trashId;
    }

    public void setTrashId(String trashId) {
        this.trashId = trashId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrashVotePK that = (TrashVotePK) o;

        if (userId != that.userId) return false;
        if (trashId != null ? !trashId.equals(that.trashId) : that.trashId != null) return false;
        if (trashId != that.trashId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (trashId != null ? trashId.hashCode() : 0);
        return result;
    }
}
