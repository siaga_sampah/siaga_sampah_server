package com.law.siagasampah.domain;

import javax.persistence.*;

/**
 * Created by arsia on 11-Apr-16.
 */
@Entity
@Table(name = "trash_vote", schema = "siaga_sampah")
@IdClass(TrashVotePK.class)
public class TrashVote {
    @Id
    @Column(name = "user_id")
    private int userId;
    @Id
    @Column(name = "trash_id")
    private String trashId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTrashId() {
        return trashId;
    }

    public void setTrashId(String trashId) {
        this.trashId = trashId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrashVote trashVote = (TrashVote) o;

        if (userId != trashVote.userId) return false;
        if (trashId != null ? !trashId.equals(trashVote.trashId) : trashVote.trashId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (trashId != null ? trashId.hashCode() : 0);
        return result;
    }
}
