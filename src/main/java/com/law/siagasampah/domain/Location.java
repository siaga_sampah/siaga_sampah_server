package com.law.siagasampah.domain;

import javax.persistence.*;

/**
 * Created by arsia on 11-Apr-16.
 */
@Entity
@Table(name = "location", schema = "siaga_sampah")
public class Location {
    @Id
    @Column(name = "trash_id", nullable = false)
    private String trashId;
    @Basic
    @Column(name = "lat")
    private double lat;
    @Basic
    @Column(name = "lng")
    private double lng;
    @Basic
    @Column(name = "location_name")
    private String locationName;

    public String getTrashId() {
        return trashId;
    }

    public void setTrashId(String trashId) {
        this.trashId = trashId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (trashId != null ? !trashId.equals(location.trashId) : location.trashId != null)
            return false;
        if (Double.compare(location.lat, lat) != 0) return false;
        if (Double.compare(location.lng, lng) != 0) return false;
        if (locationName != null ? !locationName.equals(location.locationName) : location.locationName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = 0;
        result = 31 * result + (trashId != null ? trashId.hashCode() : 0);
        temp = Double.doubleToLongBits(lat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lng);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        return result;
    }
}
