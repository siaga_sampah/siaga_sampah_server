package com.law.siagasampah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by arsia on 07-Apr-16.
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Id already registered")
public class IdAlreadyRegisteredException extends RuntimeException{
    public IdAlreadyRegisteredException(){}
}
