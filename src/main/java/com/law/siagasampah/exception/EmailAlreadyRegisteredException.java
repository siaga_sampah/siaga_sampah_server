package com.law.siagasampah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by arsia on 07-Apr-16.
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Email already registered")
public class EmailAlreadyRegisteredException extends RuntimeException{
    public EmailAlreadyRegisteredException(){}
}
