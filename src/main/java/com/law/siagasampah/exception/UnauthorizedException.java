package com.law.siagasampah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by arsia on 08-Apr-16.
 */
@ResponseStatus(value= HttpStatus.UNAUTHORIZED, reason="Unauthorized key")
public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException(){}
}