package com.law.siagasampah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by arsia on 19-Apr-16.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Item Not Found")
public class ItemNotFoundException extends RuntimeException {
    public ItemNotFoundException(){}
}
