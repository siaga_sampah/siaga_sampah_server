package com.law.siagasampah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by arsia on 03-Apr-16.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Email not registered")
public class NoSuchEmailException extends RuntimeException{
    public NoSuchEmailException(){}
}
