package com.law.siagasampah.exception;

import com.law.siagasampah.util.ErrorJson;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


/**
 * Created by arsia on 02-Apr-16.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)  // 404
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseBody
    ErrorJson handleUserNotFound() {
        return new ErrorJson(HttpStatus.NOT_FOUND, "User not found");
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)  // 403
    @ExceptionHandler(WrongPasswordException.class)
    @ResponseBody
    ErrorJson handleWrongPassword() {
        return new ErrorJson(HttpStatus.FORBIDDEN, "Wrong password");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)  // 404
    @ExceptionHandler(NoSuchEmailException.class)
    @ResponseBody
    ErrorJson handleNoSuchEmail() {
        return new ErrorJson(HttpStatus.NOT_FOUND, "Email not registered");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)  // 401
    @ExceptionHandler(NoSuchUserException.class)
    @ResponseBody
    ErrorJson handleNoSuchUser(){
        return new ErrorJson(HttpStatus.NOT_FOUND, "The user is not registered");
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)  // 403
    @ExceptionHandler(WrongOldPasswordException.class)
    @ResponseBody
    ErrorJson handleWrongOldPassword(){
        return new ErrorJson(HttpStatus.FORBIDDEN, "Wrong old password");
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)  // 403
    @ExceptionHandler(IdAlreadyRegisteredException.class)
    @ResponseBody
    ErrorJson handleIdAlreadyRegistered(){
        return new ErrorJson(HttpStatus.FORBIDDEN, "Id Already Registered");
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)  // 403
    @ExceptionHandler(EmailAlreadyRegisteredException.class)
    @ResponseBody
    ErrorJson handleEmailAlreadyRegistered(){
        return new ErrorJson(HttpStatus.FORBIDDEN, "Email Already Registered");
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)  // 401
    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    ErrorJson handleUnauthorized() {
        return new ErrorJson(HttpStatus.UNAUTHORIZED, "Unauthorized key");
    }

//    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 400
//    @ExceptionHandler(Exception.class)
//    @ResponseBody
//    ErrorJson handleBadRequest(){
//        return new ErrorJson(HttpStatus.BAD_REQUEST, "Bad Request");
//    }

    @ResponseStatus(HttpStatus.NOT_FOUND)  // 404
    @ExceptionHandler(ItemNotFoundException.class)
    @ResponseBody
    ErrorJson handleItemNotFound() {
        return new ErrorJson(HttpStatus.NOT_FOUND, "Item not found");
    }
}
