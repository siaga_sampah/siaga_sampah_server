package com.law.siagasampah.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by arsia on 07-Apr-16.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Id is not registered")
public class NoSuchUserException extends RuntimeException{
    public NoSuchUserException(){}
}
