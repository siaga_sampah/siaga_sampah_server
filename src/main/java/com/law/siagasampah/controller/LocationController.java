package com.law.siagasampah.controller;

import com.law.siagasampah.service.LocationService;
import com.law.siagasampah.util.LocationWrap;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by arsia on 25-Apr-16.
 */
@RestController
@RequestMapping("/locations")
public class LocationController {
    @Autowired
    private LocationService locationService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public List<LocationWrap> getTrashPagination(@RequestParam double lat, @RequestParam double lng) {
        return locationService.getLocation(lat, lng);
    }
}
