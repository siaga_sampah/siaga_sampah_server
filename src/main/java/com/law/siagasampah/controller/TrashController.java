package com.law.siagasampah.controller;

import com.law.siagasampah.domain.Trash;
import com.law.siagasampah.service.TrashService;
import com.law.siagasampah.service.TrashVoteService;
import com.law.siagasampah.util.GsonUtil;
import com.law.siagasampah.util.PageWrap;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by arsia on 18-Apr-16.
 */
@RestController
@RequestMapping("/trashes")
public class TrashController {
    @Autowired
    private TrashService trashService;

//    @RequestMapping(method = RequestMethod.POST)
//    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
//    public void postTrash(@RequestBody Trash trash) throws IOException {
//        trashService.postTrash(trash);
//    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public void postTrash(@RequestPart("trash")String trashJson, @RequestPart("img")String img) throws IOException {
        Trash trash = GsonUtil.getInstance().fromJson(trashJson, Trash.class);
        trash.setImage(img);
        trashService.postTrash(trash);
    }

//    @RequestMapping(path = "/upload", method = RequestMethod.POST)
//    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
//    public void uploadTrash(@RequestPart("img")String img) throws IOException {
//        trashService.upload(img);
//    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public Trash getTrash(@PathVariable("id") String id){
        return trashService.getTrash(id);
    }

    @RequestMapping(path = "/{id}/status", method = RequestMethod.PUT)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public void setStatus(@PathVariable("id") String id, @RequestParam int status){
        trashService.setStatus(id, status);
    }

    @RequestMapping(path = "trash-page/{page}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public PageWrap getTrashPagination(@PathVariable("page") int page) {
        return trashService.getTrashPagination(page, 10);
    }
}
