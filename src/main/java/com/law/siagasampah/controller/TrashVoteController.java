package com.law.siagasampah.controller;

import com.law.siagasampah.domain.TrashVote;
import com.law.siagasampah.domain.User;
import com.law.siagasampah.service.TrashVoteService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by arsia on 26-Apr-16.
 */
@RestController
@RequestMapping("/trashvotes")
public class TrashVoteController {
    @Autowired
    TrashVoteService trashVoteService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public void addVote(@RequestBody TrashVote trashVote){
        trashVoteService.addVote(trashVote);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public TrashVote isVote(@PathVariable("id") String id, @RequestParam int userId){
        return trashVoteService.isVote(id, userId);
    }
}
