package com.law.siagasampah.controller;

import com.law.siagasampah.domain.User;
import com.law.siagasampah.exception.WrongOldPasswordException;
import com.law.siagasampah.service.TrashService;
import com.law.siagasampah.service.UserService;
import com.law.siagasampah.util.PageWrap;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by arsia on 11-Apr-16.
 */
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private TrashService trashService;

    @RequestMapping(method = RequestMethod.PUT)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public User updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public User getUser(@PathVariable("id") int id){
        return userService.getUser(id);
    }

    @RequestMapping(path = "/{id}/trashes", method = RequestMethod.GET)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public PageWrap getUserTrashes(@PathVariable("id") int id, @RequestParam int page){
        return trashService.getUserTrashPagination(id, page, 10);
    }

    @RequestMapping(path = "/{id}/password", method = RequestMethod.PUT)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public void updatePassword(@PathVariable("id") int id, @RequestParam String oldPassword, @RequestParam String newPassword, HttpServletResponse response) {
        if(!userService.updatePassword(id, oldPassword, newPassword)) {
            throw new WrongOldPasswordException();
        }
    }

    @RequestMapping(path = "/signin", method = RequestMethod.POST)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public User signin(@RequestParam String email, @RequestParam String password){
        return userService.signin(email, password);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiImplicitParam(name = "Api-token", value = "338569c9e09993474964ac3c7bd88f19", dataType = "string", paramType = "header", required = true)
    public User signup(@RequestBody User user){
        return userService.signUp(user);
    }
}
