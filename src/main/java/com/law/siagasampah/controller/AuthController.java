package com.law.siagasampah.controller;

import com.law.siagasampah.exception.UnauthorizedException;
import com.law.siagasampah.service.AuthService;
import com.law.siagasampah.util.TokenJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by arsia on 11-Apr-16.
 */
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public TokenJson registerApp(@RequestParam String clientId, @RequestParam String secretKey) {
        if (authService.isAndroidKeyValid(secretKey)) return new TokenJson(authService.generateApiKey(clientId));
        else throw new UnauthorizedException();
    }
}
