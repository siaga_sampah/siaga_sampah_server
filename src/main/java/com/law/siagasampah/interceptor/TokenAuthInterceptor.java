package com.law.siagasampah.interceptor;

import com.google.gson.Gson;
import com.law.siagasampah.exception.UnauthorizedException;
import com.law.siagasampah.service.AuthService;
import com.law.siagasampah.util.ErrorJson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by arief on 28/03/2016.
 */
@Component
public class TokenAuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthService service;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if(service.isTokenValid(request.getHeader("Api-token"))){
            return true;
        }
        else {
            throw new UnauthorizedException();
        }
    }
}
