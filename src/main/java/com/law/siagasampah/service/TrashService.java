package com.law.siagasampah.service;

import com.law.siagasampah.domain.Location;
import com.law.siagasampah.domain.Trash;
import com.law.siagasampah.domain.meta.TrashMeta;
import com.law.siagasampah.exception.ItemNotFoundException;
import com.law.siagasampah.repository.LocationRepository;
import com.law.siagasampah.repository.TrashRepository;
import com.law.siagasampah.repository.UserRepository;
import com.law.siagasampah.util.Constants;
import com.law.siagasampah.util.PageWrap;
import org.apache.tomcat.util.bcel.classfile.Constant;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by arsia on 18-Apr-16.
 */
@Service
@Transactional
public class TrashService {
    @Autowired
    private TrashRepository trashRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HttpServletRequest request;

    public void postTrash(Trash trash) throws IOException {
        String uuid = UUID.randomUUID().toString();
        trash.setId(uuid);

        String[] parts = trash.getImage().split(",");
        String imageString = parts[1];

        String uploadsDir = "/";
        String path =  request.getServletContext().getRealPath(uploadsDir);
        String paths = new File(path).getParent() + "/uploads/";
        if(! new File(paths).exists())
        {
            new File(paths).mkdir();
        }
        String urlDir = Constants.UPLOAD_URL + trash.getId() + Constants.IMAGE_EXTENSION;
        String saveDir = paths + trash.getId() + Constants.IMAGE_EXTENSION;

        saveImage(imageString, saveDir);

        trash.setImage(urlDir);
        trashRepository.save(trash);
        Location loc = trash.getLocation();
        loc.setTrashId(trash.getId());
        locationRepository.save(loc);
    }

//    public String upload(String img) throws IOException {
//        String uuid = UUID.randomUUID().toString();
//
//        String[] parts = img.split(",");
//        String imageString = parts[1];
//
//        String uploadsDir = "/";
//        String path =  request.getServletContext().getRealPath(uploadsDir);
//        String paths = new File(path).getParent() + "/uploads/";
//        if(! new File(paths).exists())
//        {
//            new File(paths).mkdir();
//        }
//
//        String urlDir = Constants.UPLOAD_URL + uuid + Constants.IMAGE_EXTENSION;
//        String saveDir = paths + uuid + Constants.IMAGE_EXTENSION;
//
//        saveImage(imageString, saveDir);
//
//        return urlDir;
//    }

    public Trash getTrash(String id) {
        Trash trash = trashRepository.findOne(id);
        if(trash != null){
            return trash;
        } else {
            throw new ItemNotFoundException();
        }
    }

    public void setStatus(String id, int status) {
        Trash trash = trashRepository.findOne(id);
        trash.setStatus(status);
        trashRepository.save(trash);
    }

    public PageWrap getTrashPagination(int page, int limit) {
        Mapper mapper = new DozerBeanMapper();
        List<TrashMeta> trashesMeta = new ArrayList<>();
        List<Trash> trashes;
        trashes = trashRepository.findByPageable(new PageRequest(page, limit));

        for(Trash trash : trashes) {
            TrashMeta trashMeta = mapper.map(trash, TrashMeta.class);
            trashMeta.setUserName(userRepository.findNameById(trash.getUserId()));
            trashesMeta.add(trashMeta);
        }
        return new PageWrap<>(page, trashesMeta);
    }

    public PageWrap getUserTrashPagination(int id, int page, int limit) {
        Mapper mapper = new DozerBeanMapper();
        List<TrashMeta> trashesMeta = new ArrayList<>();
        List<Trash> trashes;
        trashes = trashRepository.findByUserIdAndPageable(id, new PageRequest(page, limit));

        for(Trash trash : trashes) {
            TrashMeta trashMeta = mapper.map(trash, TrashMeta.class);
            trashMeta.setUserName(userRepository.findNameById(trash.getUserId()));
            trashesMeta.add(trashMeta);
        }
        return new PageWrap<>(page, trashesMeta);
    }

    @Async
    private void saveImage(String data, String fileName) throws IOException{
        BufferedImage image;
        byte[] imageByte;

        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(data);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        bis.close();

        File outputfile = new File(fileName);
        ImageIO.write(image, "jpeg", outputfile);
    }
}
