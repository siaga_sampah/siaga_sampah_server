package com.law.siagasampah.service;

import com.law.siagasampah.domain.User;
import com.law.siagasampah.exception.*;
import com.law.siagasampah.repository.UserRepository;
import com.law.siagasampah.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

/**
 * Created by arsia on 11-Apr-16.
 */
@Service
@Transactional
public class UserService {
    @Autowired
    UserRepository userRepository;

    public boolean isUserValid(String email){
        return userRepository.findByEmail(email) != null;
    }

    public boolean isUserValid(User user){
        return userRepository.findById(user.getId()) != null;
    }

    public User updateUser(User user){
        if (isUserValid(user)){
            User u = userRepository.findById(user.getId());
            user.setPassword(u.getPassword());
            return userRepository.save(user);
        } else {
            throw new NoSuchUserException();
        }
    }

    public User getUser(int id) {
        User user = userRepository.findOne(id);
        if(user != null){
            return user;
        } else {
            throw new UserNotFoundException();
        }
    }

    public User signin(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (user != null){
            if(!user.getPassword().equals(hash(password))){
                throw new WrongPasswordException();
            } else {
                return user;
            }
        } else {
            throw new NoSuchEmailException();
        }
    }

    public boolean updatePassword(int id, String oldPassword, String newPassword){
        User user = userRepository.findOne(id);
        if(user != null) {
            String oldPasswordMd5 = hash(oldPassword);

            if(oldPasswordMd5.equals(user.getPassword())) {
                user.setPassword(hash(newPassword));
                userRepository.save(user);
                return true;
            }else{
                return false;
            }
        } else {
            throw new NoSuchUserException();
        }
    }

    public User signUp(User u){
        User user = userRepository.findOne(u.getId());
        if(user == null){
            user = userRepository.findByEmail(u.getEmail());
            if(user == null){
                u.setPassword(hash(u.getPassword()));
                return userRepository.save(u);
            } else {
                throw new EmailAlreadyRegisteredException();
            }
        } else {
            throw new IdAlreadyRegisteredException();
        }
    }

    public String hash(String password){
        String tmp = DigestUtils.md5DigestAsHex(password.getBytes());
        return DigestUtils.md5DigestAsHex((tmp + Constants.HASH_KEY).getBytes());
    }
}
