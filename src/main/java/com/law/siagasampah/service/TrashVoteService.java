package com.law.siagasampah.service;

import com.law.siagasampah.domain.Trash;
import com.law.siagasampah.domain.TrashVote;
import com.law.siagasampah.exception.ItemNotFoundException;
import com.law.siagasampah.repository.TrashRepository;
import com.law.siagasampah.repository.TrashVoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by arsia on 26-Apr-16.
 */
@Service
@Transactional
public class TrashVoteService {
    @Autowired
    TrashVoteRepository trashVoteRepository;

    @Autowired
    TrashRepository trashRepository;

    public void addVote(TrashVote trashVote){
        TrashVote tv = trashVoteRepository.findByUserIdAndTrashId(trashVote.getUserId(), trashVote.getTrashId());
        if (tv == null){
            trashVoteRepository.save(trashVote);

            Trash trash = trashRepository.findOne(trashVote.getTrashId());
            int vote = trash.getVote();
            trash.setVote(vote + 1);
            trashRepository.save(trash);
        }
    }

    public TrashVote isVote(String trashId, int userId){
        TrashVote trashVote = trashVoteRepository.findByUserIdAndTrashId(userId, trashId);
        if (trashVote != null){
            return trashVote;
        } else {
            throw new ItemNotFoundException();
        }
    }
}
