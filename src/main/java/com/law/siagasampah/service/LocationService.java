package com.law.siagasampah.service;

import com.law.siagasampah.domain.Location;
import com.law.siagasampah.domain.Trash;
import com.law.siagasampah.domain.User;
import com.law.siagasampah.repository.LocationRepository;
import com.law.siagasampah.repository.TrashRepository;
import com.law.siagasampah.repository.UserRepository;
import com.law.siagasampah.util.LocationWrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arsia on 25-Apr-16.
 */
@Service
@Transactional
public class LocationService {
    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private TrashRepository trashRepository;

    @Autowired
    private UserRepository userRepository;

    public List<LocationWrap> getLocation(double lat, double lng){
        List<LocationWrap> locationWrap = new ArrayList<>();
        List<Location> locations;

        locations = locationRepository.findByPageable(lat, lng);
        for(Location location : locations) {
            Trash trash = trashRepository.findOne(location.getTrashId());
            User user = userRepository.findOne(trash.getUserId());
            LocationWrap wrap = new LocationWrap(user.getName(), location);
            locationWrap.add(wrap);
        }

        return locationWrap;
    }
}
