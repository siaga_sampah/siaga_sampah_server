package com.law.siagasampah.service;

import com.law.siagasampah.domain.AccessToken;
import com.law.siagasampah.repository.AccessTokenRepository;
import com.law.siagasampah.util.Constants;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import java.io.UnsupportedEncodingException;

/**
 * Created by arief on 27/03/2016.
 */

@Service
public class AuthService {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    private static final String CHARACTER_CODE = "UTF-8";

    public boolean isAndroidKeyValid(String key) {
        return key.equals(Constants.ANDROID_SECRET_KEY);
    }

    public String generateApiKey(String clientId) {
        String data = clientId + System.currentTimeMillis();
        String key = null;
        try {
            Mac hmac256 = HmacUtils.getHmacSha256(Constants.ANDROID_SECRET_KEY.getBytes(CHARACTER_CODE));
            key = DigestUtils.md5Hex(Base64.encodeBase64(hmac256.doFinal(data.getBytes(CHARACTER_CODE))));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return key;
        }
        AccessToken ac = accessTokenRepository.findByClientId(clientId);
        if(ac == null){
            accessTokenRepository.save(new AccessToken(key, clientId));
        } else {
            ac.setToken(key);
            accessTokenRepository.save(ac);
        }
        return key;
    }

    public boolean isTokenValid(String token){
        return accessTokenRepository.findByToken(token) != null;
    }
}