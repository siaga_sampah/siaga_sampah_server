package com.law.siagasampah.util;

import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * Created by arsia on 28-Mar-16.
 */
public class ErrorJson {
    public Integer status;
    public String error;
    public String message;
    public String timestamp;
    public String trace;

    public ErrorJson(HttpStatus status, String message){
        this.status = status.value();
        this.error = status.name();
        this.message = message;
    }

    public ErrorJson(int status, Map<String, Object> errorAttributes){
        this.status = status;
        this.error = (String) errorAttributes.get("error");
        this.message = (String) errorAttributes.get("message");
        this.timestamp = errorAttributes.get("timestamp").toString();
        this.trace = (String) errorAttributes.get("trace");
    }
}
