package com.law.siagasampah.util;

/**
 * Created by arief on 27/03/2016.
 */
public class Constants {

    public static final String ANDROID_SECRET_KEY = "OVCsOfGvMPGNfqHVDofl5ZHTKrxRZxddQn1vB2muzsxge3vGOLc8H4mULKYB45ty";
    public static final String HASH_KEY = "fa123a50e862390b68f04f856369e521";

    public static final String UPLOAD_URL = "http://104.155.216.220:8080/uploads/";
    public static final String IMAGE_EXTENSION = ".jpeg";

    public static final int TRASH_STATUS_CLEANED = 1;

}
