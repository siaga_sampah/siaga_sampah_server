package com.law.siagasampah.util;

import com.law.siagasampah.domain.Location;

import java.util.List;

/**
 * Created by arsia on 26-Apr-16.
 */
public class LocationWrap {
    private String name;
    private Location location;

    public LocationWrap(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
