package com.law.siagasampah.util;

/**
 * Created by arsia on 08-Apr-16.
 */
public class TokenJson {
    private String token;

    public TokenJson() {
    }

    public TokenJson(String token) {

        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
