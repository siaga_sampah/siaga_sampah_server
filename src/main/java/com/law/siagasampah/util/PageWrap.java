package com.law.siagasampah.util;

import java.util.List;

/**
 * Created by arsia on 19-Apr-16.
 */
public class PageWrap<T> {
    private int page;
    private List<T> items;

    public PageWrap(int page, List<T> items) {
        this.page = page;
        this.items = items;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
