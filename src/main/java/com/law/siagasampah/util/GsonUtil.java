package com.law.siagasampah.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by admin on 30/01/2016.
 */
public class GsonUtil {

    private static Gson gson;

    public static Gson getInstance() {
        if (gson == null) {
            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        }
        return gson;
    }
}
